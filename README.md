## qb_hand (noetic) - 3.0.3-1

The packages in the `qb_hand` repository were released into the `noetic` distro by running `/usr/bin/bloom-release qb_hand --track noetic --rosdistro noetic --edit` on `Fri, 28 Jul 2023 12:18:15 -0000`

These packages were released:
- `qb_hand`
- `qb_hand_control`
- `qb_hand_description`
- `qb_hand_gazebo`
- `qb_hand_hardware_interface`

Version of package(s) in repository `qb_hand`:

- upstream repository: https://bitbucket.org/qbrobotics/qbhand-ros.git
- release repository: https://bitbucket.org/qbrobotics/qbhand-ros-release.git
- rosdistro version: `3.0.2-1`
- old version: `3.0.2-1`
- new version: `3.0.3-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## qb_hand (melodic) - 3.0.2-2

The packages in the `qb_hand` repository were released into the `melodic` distro by running `/usr/local/bin/bloom-release qb_hand --track melodic --rosdistro melodic` on `Mon, 09 Jan 2023 10:03:13 -0000`

These packages were released:
- `qb_hand`
- `qb_hand_control`
- `qb_hand_description`
- `qb_hand_gazebo`
- `qb_hand_hardware_interface`

Version of package(s) in repository `qb_hand`:

- upstream repository: https://bitbucket.org/qbrobotics/qbhand-ros.git
- release repository: https://bitbucket.org/qbrobotics/qbhand-ros-release.git
- rosdistro version: `2.0.0-1`
- old version: `3.0.2-1`
- new version: `3.0.2-2`

Versions of tools used:

- bloom version: `0.10.7`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.21.0`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## qb_hand (noetic) - 3.0.2-1

The packages in the `qb_hand` repository were released into the `noetic` distro by running `/usr/local/bin/bloom-release qb_hand --track noetic --rosdistro noetic` on `Thu, 22 Sep 2022 10:04:32 -0000`

These packages were released:
- `qb_hand`
- `qb_hand_control`
- `qb_hand_description`
- `qb_hand_gazebo`
- `qb_hand_hardware_interface`

Version of package(s) in repository `qb_hand`:

- upstream repository: https://bitbucket.org/qbrobotics/qbhand-ros.git
- release repository: https://bitbucket.org/qbrobotics/qbhand-ros-release.git
- rosdistro version: `2.2.2-1`
- old version: `2.2.2-1`
- new version: `3.0.2-1`

Versions of tools used:

- bloom version: `0.10.7`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.21.0`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## qb_hand (melodic) - 3.0.2-1

The packages in the `qb_hand` repository were released into the `melodic` distro by running `/usr/local/bin/bloom-release qb_hand --track melodic --rosdistro melodic` on `Thu, 22 Sep 2022 10:01:30 -0000`

These packages were released:
- `qb_hand`
- `qb_hand_control`
- `qb_hand_description`
- `qb_hand_gazebo`
- `qb_hand_hardware_interface`

Version of package(s) in repository `qb_hand`:

- upstream repository: https://bitbucket.org/qbrobotics/qbhand-ros.git
- release repository: https://bitbucket.org/qbrobotics/qbhand-ros-release.git
- rosdistro version: `2.0.0-1`
- old version: `2.2.2-1`
- new version: `3.0.2-1`

Versions of tools used:

- bloom version: `0.10.7`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.21.0`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## qb_hand (noetic) - 2.2.2-1

The packages in the `qb_hand` repository were released into the `noetic` distro by running `/usr/local/bin/bloom-release qb_hand --track noetic --rosdistro noetic --new-track` on `Mon, 30 Aug 2021 12:29:55 -0000`

These packages were released:
- `qb_hand`
- `qb_hand_control`
- `qb_hand_description`
- `qb_hand_gazebo`
- `qb_hand_hardware_interface`

Version of package(s) in repository `qb_hand`:

- upstream repository: https://bitbucket.org/qbrobotics/qbhand-ros.git
- release repository: unknown
- rosdistro version: `null`
- old version: `null`
- new version: `2.2.2-1`

Versions of tools used:

- bloom version: `0.10.7`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.21.0`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## qb_hand (melodic) - 2.2.2-1

The packages in the `qb_hand` repository were released into the `melodic` distro by running `/usr/local/bin/bloom-release qb_hand --track melodic --rosdistro melodic` on `Mon, 30 Aug 2021 11:02:20 -0000`

These packages were released:
- `qb_hand`
- `qb_hand_control`
- `qb_hand_description`
- `qb_hand_gazebo`
- `qb_hand_hardware_interface`

Version of package(s) in repository `qb_hand`:

- upstream repository: https://bitbucket.org/qbrobotics/qbhand-ros.git
- release repository: https://bitbucket.org/qbrobotics/qbhand-ros-release.git
- rosdistro version: `2.0.0-1`
- old version: `2.0.0-1`
- new version: `2.2.2-1`

Versions of tools used:

- bloom version: `0.10.7`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.21.0`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## qb_hand (melodic) - 2.0.0-1

The packages in the `qb_hand` repository were released into the `melodic` distro by running `/usr/bin/bloom-release qb_hand --track melodic --rosdistro melodic` on `Fri, 01 Jun 2018 13:39:41 -0000`

These packages were released:
- `qb_hand`
- `qb_hand_control`
- `qb_hand_description`
- `qb_hand_hardware_interface`

Version of package(s) in repository `qb_hand`:

- upstream repository: https://bitbucket.org/qbrobotics/qbhand-ros.git
- release repository: https://bitbucket.org/qbrobotics/qbhand-ros-release.git
- rosdistro version: `2.0.0-0`
- old version: `2.0.0-0`
- new version: `2.0.0-1`

Versions of tools used:

- bloom version: `0.6.4`
- catkin_pkg version: `0.4.2`
- rosdep version: `0.12.2`
- rosdistro version: `0.6.8`
- vcstools version: `0.1.40`


## qb_hand (lunar) - 2.0.0-1

The packages in the `qb_hand` repository were released into the `lunar` distro by running `/usr/bin/bloom-release qb_hand --track lunar --rosdistro lunar` on `Fri, 01 Jun 2018 13:33:24 -0000`

These packages were released:
- `qb_hand`
- `qb_hand_control`
- `qb_hand_description`
- `qb_hand_hardware_interface`

Version of package(s) in repository `qb_hand`:

- upstream repository: https://bitbucket.org/qbrobotics/qbhand-ros.git
- release repository: unknown
- rosdistro version: `null`
- old version: `2.0.0-0`
- new version: `2.0.0-1`

Versions of tools used:

- bloom version: `0.6.4`
- catkin_pkg version: `0.4.2`
- rosdep version: `0.12.2`
- rosdistro version: `0.6.8`
- vcstools version: `0.1.40`


## qb_hand (melodic) - 2.0.0-0

The packages in the `qb_hand` repository were released into the `melodic` distro by running `/usr/bin/bloom-release qb_hand --track melodic --rosdistro melodic --new-track` on `Thu, 31 May 2018 15:23:10 -0000`

These packages were released:
- `qb_hand`
- `qb_hand_control`
- `qb_hand_description`
- `qb_hand_hardware_interface`

Version of package(s) in repository `qb_hand`:

- upstream repository: https://bitbucket.org/qbrobotics/qbhand-ros.git
- release repository: unknown
- rosdistro version: `null`
- old version: `null`
- new version: `2.0.0-0`

Versions of tools used:

- bloom version: `0.6.4`
- catkin_pkg version: `0.4.2`
- rosdep version: `0.12.2`
- rosdistro version: `0.6.8`
- vcstools version: `0.1.40`


## qb_hand (lunar) - 2.0.0-0

The packages in the `qb_hand` repository were released into the `lunar` distro by running `/usr/bin/bloom-release qb_hand --track lunar --rosdistro lunar --new-track` on `Thu, 31 May 2018 15:16:18 -0000`

These packages were released:
- `qb_hand`
- `qb_hand_control`
- `qb_hand_description`
- `qb_hand_hardware_interface`

Version of package(s) in repository `qb_hand`:

- upstream repository: https://bitbucket.org/qbrobotics/qbhand-ros.git
- release repository: unknown
- rosdistro version: `null`
- old version: `null`
- new version: `2.0.0-0`

Versions of tools used:

- bloom version: `0.6.4`
- catkin_pkg version: `0.4.2`
- rosdep version: `0.12.2`
- rosdistro version: `0.6.8`
- vcstools version: `0.1.40`


## qb_hand (kinetic) - 2.0.0-0

The packages in the `qb_hand` repository were released into the `kinetic` distro by running `/usr/bin/bloom-release qb_hand --track kinetic --rosdistro kinetic` on `Thu, 31 May 2018 10:31:47 -0000`

These packages were released:
- `qb_hand`
- `qb_hand_control`
- `qb_hand_description`
- `qb_hand_hardware_interface`

Version of package(s) in repository `qb_hand`:

- upstream repository: https://bitbucket.org/qbrobotics/qbhand-ros.git
- release repository: https://bitbucket.org/qbrobotics/qbhand-ros-release.git
- rosdistro version: `1.0.6-0`
- old version: `1.0.6-0`
- new version: `2.0.0-0`

Versions of tools used:

- bloom version: `0.6.4`
- catkin_pkg version: `0.4.2`
- rosdep version: `0.12.2`
- rosdistro version: `0.6.8`
- vcstools version: `0.1.40`


## qb_hand (kinetic) - 1.0.6-0

The packages in the `qb_hand` repository were released into the `kinetic` distro by running `/usr/bin/bloom-release --rosdistro kinetic --track kinetic qb_hand` on `Fri, 24 Nov 2017 11:58:49 -0000`

These packages were released:
- `qb_hand`
- `qb_hand_control`
- `qb_hand_description`
- `qb_hand_hardware_interface`

Version of package(s) in repository `qb_hand`:

- upstream repository: https://bitbucket.org/qbrobotics/qbhand-ros.git
- release repository: https://bitbucket.org/qbrobotics/qbhand-ros-release.git
- rosdistro version: `1.0.5-0`
- old version: `1.0.5-0`
- new version: `1.0.6-0`

Versions of tools used:

- bloom version: `0.6.1`
- catkin_pkg version: `0.3.9`
- rosdep version: `0.11.8`
- rosdistro version: `0.6.2`
- vcstools version: `0.1.39`


## qb_hand (kinetic) - 1.0.5-0

The packages in the `qb_hand` repository were released into the `kinetic` distro by running `/usr/bin/bloom-release --rosdistro kinetic --track kinetic qb_hand` on `Tue, 27 Jun 2017 13:07:03 -0000`

These packages were released:
- `qb_hand`
- `qb_hand_control`
- `qb_hand_description`
- `qb_hand_hardware_interface`

Version of package(s) in repository `qb_hand`:

- upstream repository: https://bitbucket.org/qbrobotics/qbhand-ros.git
- release repository: https://bitbucket.org/qbrobotics/qbhand-ros-release.git
- rosdistro version: `1.0.4-0`
- old version: `1.0.4-0`
- new version: `1.0.5-0`

Versions of tools used:

- bloom version: `0.5.26`
- catkin_pkg version: `0.3.5`
- rosdep version: `0.11.5`
- rosdistro version: `0.6.2`
- vcstools version: `0.1.39`


## qb_hand (kinetic) - 1.0.4-0

The packages in the `qb_hand` repository were released into the `kinetic` distro by running `/usr/bin/bloom-release --rosdistro kinetic --track kinetic qb_hand` on `Fri, 23 Jun 2017 16:00:17 -0000`

These packages were released:
- `qb_hand`
- `qb_hand_control`
- `qb_hand_description`
- `qb_hand_hardware_interface`

Version of package(s) in repository `qb_hand`:

- upstream repository: https://bitbucket.org/qbrobotics/qbhand-ros.git
- release repository: https://bitbucket.org/qbrobotics/qbhand-ros-release.git
- rosdistro version: `1.0.1-0`
- old version: `1.0.1-0`
- new version: `1.0.4-0`

Versions of tools used:

- bloom version: `0.5.26`
- catkin_pkg version: `0.3.5`
- rosdep version: `0.11.5`
- rosdistro version: `0.6.2`
- vcstools version: `0.1.39`


## qb_hand (kinetic) - 1.0.1-0

The packages in the `qb_hand` repository were released into the `kinetic` distro by running `/usr/bin/bloom-release --rosdistro kinetic --track kinetic qb_hand --edit` on `Mon, 19 Jun 2017 15:51:03 -0000`

These packages were released:
- `qb_hand`
- `qb_hand_control`
- `qb_hand_description`
- `qb_hand_hardware_interface`

Version of package(s) in repository `qb_hand`:

- upstream repository: https://bitbucket.org/qbrobotics/qbhand-ros.git
- release repository: unknown
- rosdistro version: `null`
- old version: `null`
- new version: `1.0.1-0`

Versions of tools used:

- bloom version: `0.5.26`
- catkin_pkg version: `0.3.5`
- rosdep version: `0.11.5`
- rosdistro version: `0.6.2`
- vcstools version: `0.1.39`


